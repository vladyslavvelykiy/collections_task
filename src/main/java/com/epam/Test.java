package com.epam;

import com.epam.collections.MyyDeque;
import com.epam.collections.TwoStringsContainer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class Test {
    private TwoStringsContainer twoStrContainer;
    private MyyDeque md;
    private static Logger logger = LogManager.getLogger(TwoStringsContainer.class);

    public Test() {
    }

    public void testTwoStrContainer() {
        logger.trace("- - - - - Container for 2 lines - - - - - ");
        testArraySort();
        testCollectionSort();
    }

    public void testMyDeque(){
        logger.trace("- - - - - Deque - - - - - ");
        md = new MyyDeque();
        md.addFirst(4);
        md.addFirst(3);
        md.addFirst(4);
        md.addLast(19);
        md.addFirst(6);
        md.addFirst(9);
        logger.trace("Deque after adding: " +  md.toString());
        logger.trace("Is empty?: " + md.isEmpty());
        logger.trace("Size is: " + md.size());
        md.offerFirst(4);
        md.offerLast(9);
        logger.trace("After offerFirst/Last: " + md.toString());
        md.removeFirst();
        md.removeLast();
        logger.trace("After removeFirst/Last: " + md.toString());
        Object f = md.peekFirst();
        Object l = md.peekLast();
        logger.trace("After peekFirst/Last: " + md.toString() + " Objects got: " + f + " " + l );
         Object k = md.pollFirst();
         Object m = md.pollLast();
        logger.trace("After pollFirst/Last: " + md.toString() + " Objects got: " + k + " " + m );
        Object p = md.pop();
        logger.trace("After pop: " + md.toString() + " Objects got: " + p );
        md.push(100);
        logger.trace("After push: " + md.toString());
        logger.trace("Get first: " + md.getFirst() + " Get last: " + md.getLast());
        md.removeFirstOccurrence(4);
        md.removeLastOccurrence(4);
        logger.trace("After removeLast/FirstOccurrence (4, 4): " +  md.toString());
    }


    private void testArraySort() {
        TwoStringsContainer[] strings = new TwoStringsContainer[7];
        strings[0] = new TwoStringsContainer("b", "h");
        strings[1] = new TwoStringsContainer("g", "a");
        strings[2] = new TwoStringsContainer("z", "x");
        strings[3] = new TwoStringsContainer("v", "c");
        strings[4] = new TwoStringsContainer("u", "s");
        strings[5] = new TwoStringsContainer("a", "z");
        strings[6] = new TwoStringsContainer("c", "v");
        logger.trace("Arrays: ");
        logger.trace("Before sorting: " + Arrays.toString(strings));
        Arrays.sort(strings);
        logger.trace("After sorting by first string: " + Arrays.toString(strings));
        Comparator<TwoStringsContainer> comparator =
                (first, second) -> first.getSecondStr().compareTo(second.getSecondStr());
        Arrays.sort(strings, comparator);
        logger.trace("After sorting by second string: " + Arrays.toString(strings));
    }

    private void testCollectionSort() {
        List<TwoStringsContainer> list = new ArrayList<>();
        list.add(new TwoStringsContainer("b", "h"));
        list.add(new TwoStringsContainer("g", "a"));
        list.add(new TwoStringsContainer("z", "x"));
        list.add(new TwoStringsContainer("v", "c"));
        list.add(new TwoStringsContainer("u", "s"));
        list.add(new TwoStringsContainer("a", "z"));
        list.add(new TwoStringsContainer("c", "v"));
        logger.trace("Collections: ");
        logger.trace("Before sorting:  " + list);
        Collections.sort(list);
        logger.trace("After sorting by first string: " + list);
        Comparator<TwoStringsContainer> comparator =
                (first, second) -> first.getSecondStr().compareTo(second.getSecondStr());
        list.sort(comparator);
        logger.trace("After sorting by second string: " + list);
    }
}
