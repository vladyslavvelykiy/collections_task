package com.epam.collections;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class MyyDeque {
    private Object[] deque;
    private int capacity;

    public MyyDeque() {
        capacity = 0;
        deque = new Object[capacity];
    }


    public void addFirst(Object o) {
        capacity++;
        addWithIndex(1, o);
    }

    public void addLast(Object o) {
        capacity++;
        addWithIndex(capacity, o);
    }

    public Object removeFirst() {
        capacity--;
        if (capacity <= 0) {
            throw new NoSuchElementException();
        }
        return removeWithIndex(1);
    }

    public Object removeLast() {
        capacity--;
        if (capacity < 0) {
            throw new NoSuchElementException();
        }
        return removeWithIndex(capacity);
    }

    public Object pollFirst() {
        capacity--;
        if (capacity <= 0) {
            return null;
        }
        return removeWithIndex(1);
    }

    public Object pollLast() {
        capacity--;
        if (capacity <= 0) {
            return null;
        }
        return removeWithIndex(capacity);
    }

    public Object getFirst() {
        if ((capacity <= 0)) {
            throw new NoSuchElementException();
        }
        return deque[0];
    }

    public Object getLast() {
        if (capacity <= 0) {
            throw new NoSuchElementException();
        }
        return deque[capacity - 1];
    }

    public Object peekFirst() {
        if (capacity <= 0) {
            return null;
        } else {
            return deque[0];
        }
    }

    public Object peekLast() {
        if (capacity <= 0) {
            return null;
        } else {
            return deque[capacity - 1];
        }
    }

    //
    public int size() {
        return capacity;
    }

    //
    public boolean isEmpty() {
        if (capacity <= 0) {
            return true;
        } else
            return false;
    }

    //
    public Object pop() {
        if (capacity <= 0) {
            throw new NoSuchElementException();
        } else {
            capacity--;
            return removeWithIndex(1);
        }

    }

    public void push(Object o) {
        capacity++;
        addWithIndex(1, o);
    }

    private void addWithIndex(int index, Object o) {
        int i;
        Object[] temp = new Object[capacity];

        if (index == 1) {
            i = 1;
        } else
            i = 0;

        for (Object e : deque) {
            temp[i] = e;
            i++;
        }
        temp[index - 1] = o;
        deque = Arrays.copyOf(temp, capacity);
    }

    private Object removeWithIndex(int index) {

        int i = 0;
        Object[] temp = new Object[capacity];
        Object value;

        if (index == capacity) {
            for (int j = 0; j < deque.length - 1; j++) {
                temp[i] = deque[j];
                i++;
            }
            value = deque[index];
        } else {
            for (int j = 1; j < deque.length; j++) {
                temp[i] = deque[j];
                i++;
            }
            value = deque[index-1];
        }


        deque = Arrays.copyOf(temp, capacity);
        if (index == 0) {
            return null;
        } else
            return value;
    }

    public boolean offerFirst(Object o) {
        addFirst(o);
        return true;
    }

    public boolean offerLast(Object o) {
        addLast(o);
        return true;
    }


    public boolean removeFirstOccurrence(Object o) {
        if ((findObject(o) == -1) || (capacity <= 0)) {
            return false;
        } else {
            removeObject(o);
            return true;
        }
    }


    public boolean removeLastOccurrence(Object o) {
        if ((findLastObject(o) == -1) || (capacity <= 0)) {
            return false;
        } else {
            removeLastObject(o);
            return true;
        }
    }

    private int findObject(Object o) {
        int i = 0;
        for (Object e : deque) {
            if (o == e) {
                return i;
            }
            i++;
        }
        return -1;
    }

    private int findLastObject(Object o){
        int i = 0;
        int indexOfLastElement = -1;
        for (Object e: deque) {
            if(o==e){
                indexOfLastElement = i;
            }
            i++;
        }
        return indexOfLastElement;
    }
    private void removeLastObject(Object o){
        Object[] temp = deque;
        int j = 0;
        capacity--;

        for (int i = 0; i < deque.length; i++) {
            if (i == findLastObject(o)) {
            } else {
                temp[j] = deque[i];
                j++;
            }
        }
        deque = Arrays.copyOf(temp, capacity);
    }
    private void removeObject(Object o) {
        Object[] temp = deque;
        int j = 0;
        capacity--;
        int counter = 0;
        for (int i = 0; i < deque.length; i++) {
            if (i == findObject(o)&&(counter!=1)) {
                counter = 1;
            } else {
                temp[j] = deque[i];
                j++;
            }
        }
        deque = Arrays.copyOf(temp, capacity);
    }

    @Override
    public String toString() {
        return "Deque=" + Arrays.toString(deque);
    }
}
