package com.epam.collections;

public class TwoStringsContainer implements Comparable<TwoStringsContainer> {

    private String firstStr;
    private String secondStr;

    public TwoStringsContainer(String firstStr, String secondStr) {
        this.firstStr = firstStr;
        this.secondStr = secondStr;
    }

    @Override
    public String toString() {
        return '\'' + firstStr + " " +  secondStr + '\'';
    }

    public String getFirstStr() {
        return firstStr;
    }

    public String getSecondStr() {
        return secondStr;
    }

    @Override
    public int compareTo(TwoStringsContainer container) {
        return firstStr.compareTo(container.firstStr);
    }
}
